<div align="center">
  <br />
  <h1>NoteLab</h1>
  <blockquote>
  <p>Application de gestion de password faite en React/Electron</p>
  </blockquote>
  <br />
  <img src="https://forthebadge.com/images/badges/built-with-love.svg" />
  <br />
  <img src="https://forthebadge.com/images/badges/made-with-javascript.svg">
</div>

---

## About

Projet perso

Lancement du projet en mode DEV
```
yarn electron:start
```

Commande pour build [mac,win,linux]
```
 yarn electron:package:mac
```
