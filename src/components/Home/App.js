import './App.css';
import React, {useState} from 'react';
import {Button, Input, Space} from 'antd';
import Text from "antd/lib/typography/Text";

export default function App(props) {
    const [password, setPassword] = useState('');
    const pw = "finlab"

    const handleClick = (value)=>{
        if(pw === value){
            window.location.href='/panel'
        }
    }
        return (
            <div className="App" style={{marginTop:"30%"}}>
                <h1 style={{textColor:"white"}}><Text  style={{color:"white"}} type="secondary">NoteLab</Text></h1>
                <Space direction="horizontal">
                    <Input.Password placeholder="Password" value={password} onChange={event => setPassword(event.target.value)}/>
                    <Button onClick={() => handleClick(password)}>Validate</Button>
                </Space>
            </div>
        );
}
