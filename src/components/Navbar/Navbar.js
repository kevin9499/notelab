import React from 'react';
import {Menu} from 'antd';
import {Link} from 'react-router-dom';

export default class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <Menu onClick={this.handleClick} mode="vertical" theme="dark">
          <Menu.Item key="mail">
            <Link to="/">Home</Link>
          </Menu.Item>
        </Menu>
    );
  }
}