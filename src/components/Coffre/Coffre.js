import './Coffre.css';
import React, {useEffect, useState} from 'react';
import {Button, Card, Col, Input, Row} from "antd";
import {DeleteTwoTone, SettingTwoTone} from "@ant-design/icons";
import Modal from "antd/es/modal/Modal";
import {loadSavedData, removeDataInStorage, saveDataInStorage, updateDataInStorage} from "../../renderer";
import {HANDLE_REMOVE_DATA, HANDLE_UPDATE_DATA} from "../../utils/constants";

const {ipcRenderer} = window.require('electron');
const {HANDLE_FETCH_DATA, HANDLE_SAVE_DATA} = require('../../utils/constants.js')

export default function Coffre(props) {

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [title, setTitle] = useState('');
    const [id, setId] = useState(0);
    const [coffre, setCoffre] = useState([]);
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    const showModal = (id, title, login, password) => {
        setId(id);
        setTitle(title)
        setLogin(login)
        setPassword(password)
        setIsModalVisible(true)
    };


    useEffect(() => {
        loadSavedData()
    }, [])

    useEffect(() => {
        ipcRenderer.on(HANDLE_FETCH_DATA, handleReceiveData)
        return () => {
            ipcRenderer.removeListener(HANDLE_FETCH_DATA, handleReceiveData)
        }
    })

    const handleReceiveData = (event, data) => {
        console.log('handle')
        setCoffre(data.message)
    }

    useEffect(() => {
        ipcRenderer.on(HANDLE_REMOVE_DATA, handleRemoveItem)
        return () => {
            ipcRenderer.removeListener(HANDLE_REMOVE_DATA, handleRemoveItem)
        }
    })

    const handleRemoveItem = (event, data) => {
        setCoffre(data.message)
    }

    const removeCoffre = (id) => {
        removeDataInStorage(id)
    };

    useEffect(() => {
        ipcRenderer.on(HANDLE_SAVE_DATA, handleNewItem)
        return () => {
            ipcRenderer.removeListener(HANDLE_SAVE_DATA, handleNewItem)
        }
    })

    const handleNewItem = (event, data) => {
        setCoffre(data.message)
    }

    useEffect(() => {
        ipcRenderer.on(HANDLE_UPDATE_DATA, handleUpdateItem)
        return () => {
            ipcRenderer.removeListener(HANDLE_UPDATE_DATA, handleUpdateItem)
        }
    })

    const handleUpdateItem = (event, data) => {
        setCoffre(data.message)
    }

    const handleOk = (title, login, password) => {
        let item = {id, title, login, password}
        updateDataInStorage(item)
        setIsModalVisible(false)
    };
    const create = () => {
        showModal(Date.now(), '', '', '')
        saveDataInStorage({id: Date.now(), title: '', login: '', password: ''})
    };

    const handleCancel = () => {
        setTitle('')
        setLogin('')
        setPassword('')
        setIsModalVisible(false)
    };

    const cofre = coffre.map((value, key) => {
        return <Col span={8}>
            <Card key={key} title={value.title} bordered={false} hoverable
                  style={{backgroundColor: "white", marginBottom: "10px"}}>
                <Button onClick={() => {navigator.clipboard.writeText(value.login)}} type="secondary" style={{width: "215px"}}>{value.login}</Button>
                <Button onClick={() => {navigator.clipboard.writeText(value.password)}} type="secondary" style={{width: "215px", marginTop: "3px"}}>{value.password}</Button>
                <p style={{marginLeft: "40%", fontSize: "20px"}}>
                    <SettingTwoTone onClick={() => showModal(value.id, value.title, value.login, value.password)}/>
                    <DeleteTwoTone style={{marginLeft: "10%"}} onClick={() => removeCoffre(value.id)}/>
                </p>

            </Card>
        </Col>
    });

    return (
        <div>
            <center><h1 style={{color: "white"}}>Coffre</h1></center>
            <Row gutter={24} style={{margin: 10}}>
                {cofre}
            </Row>
            <Button style={{marginLeft: "120px", marginTop: "60px", height: "60px", width: "60px"}} type="primary"
                    shape="circle" onClick={() => create()}>+</Button>
            <Modal visible={isModalVisible} footer={null} onCancel={handleCancel}>
                <label>Title</label>
                <Input value={title} style={{width: "95%"}} onChange={event => setTitle(event.target.value)}/>
                <label>Login</label>
                <Input value={login} style={{width: "95%"}} onChange={event => setLogin(event.target.value)}/>
                <label>Password</label>
                <Input value={password} style={{width: "95%"}} onChange={event => setPassword(event.target.value)}/>
                <Button onClick={handleCancel} style={{marginLeft: "30%", marginTop: "20px", marginRight: "5%"}}
                        type="danger">Cancel</Button>
                <Button onClick={() => handleOk(title, login, password)} style={{backgroundColor: "green"}}
                        type="primary">Save</Button>
            </Modal>
        </div>
    );
}