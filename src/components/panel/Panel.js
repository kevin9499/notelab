import './Panel.css';
import React, {useState} from 'react';
import {Menu} from 'antd';
import {LockOutlined, FileOutlined} from '@ant-design/icons';
import Note from "../Note/Note";
import Coffre from "../Coffre/Coffre";
import Layout, {Content} from "antd/es/layout/layout";

const {Sider} = Layout;

export default function Panel(props) {
    const [panel, setPanel] = useState('note');

    const handleClick = (value) => {
        setPanel(value)
    }

    const showPanel = () => {
        if (panel === "note") {
            return <Note/>
        }
        if (panel === "coffre") {
            return <Coffre/>
        }
    }

    return (
        <>
            <Layout>
                <Sider width={110} style={{height: "2500px"}}>
                    <Menu mode="vertical"
                          theme="dark"
                          inlineCollapsed={true}
                    >
                        <Menu.Item key="1" icon={<LockOutlined style={{fontSize: "25px"}}/>}
                                   onClick={() => handleClick('coffre')}>Coffre
                        </Menu.Item>
                        <Menu.Item key="2" icon={<FileOutlined style={{fontSize: "25px"}}/>}
                                   onClick={() => handleClick('note')}>Note
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Content style={{backgroundColor: "#222b33"}}>
                    {showPanel()}
                </Content>
            </Layout>
        </>
    );
}

