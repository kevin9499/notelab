module.exports = {
    FETCH_DATA_FROM_STORAGE: 'fetch-data-from-storage',
    HANDLE_FETCH_DATA: 'handle-fetch-data',
    SAVE_DATA_IN_STORAGE: 'save-data-in-storage',
    HANDLE_SAVE_DATA: 'handle-save-data',
    UPDATE_DATA_IN_STORAGE: 'update-data-in-storage',
    HANDLE_UPDATE_DATA: 'handle-update-data',
    REMOVE_DATA_IN_STORAGE: 'remove-data-in-storage',
    HANDLE_REMOVE_DATA: 'handle-remove-data',
}
