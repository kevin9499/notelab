import {REMOVE_DATA_IN_STORAGE, UPDATE_DATA_IN_STORAGE} from "./utils/constants";

const {ipcRenderer} = window.require('electron');
const {FETCH_DATA_FROM_STORAGE, SAVE_DATA_IN_STORAGE} = require("./utils/constants.js")

export function loadSavedData() {
    ipcRenderer.send(FETCH_DATA_FROM_STORAGE, "coffre");
}

export function saveDataInStorage(item) {
    ipcRenderer.send(SAVE_DATA_IN_STORAGE, item);
}

export function updateDataInStorage(item) {
    ipcRenderer.send(UPDATE_DATA_IN_STORAGE, item);
}

export function removeDataInStorage(item) {
    ipcRenderer.send(REMOVE_DATA_IN_STORAGE, item);
}