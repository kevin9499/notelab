import React from 'react';
import {BrowserRouter as Router, Routes, Route}
  from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import Home from '../components/Home/App';
import {Layout} from 'antd';
import Navbar from '../components/Navbar/Navbar';
import Panel from "../components/panel/Panel";


const {Content} = Layout;
export default function AppRouter(props) {


  return (
        <div>
        <Router>
            {/*<Navbar/>*/}
          <Routes>
            <Route exact path="/" element={<Home/>}/>
            <Route path="/panel" element={<Panel/>}/>
            <Route path="/coffre" element={<Home/>}/>
            <Route path="/note" element={<Home/>}/>
          </Routes>
            {/*<Footer/>*/}
        </Router>
        </div>
  );
}