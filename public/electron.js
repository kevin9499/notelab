const path = require('path');
const {app, BrowserWindow, ipcMain} = require('electron/remote');
const isDev = require('electron-is-dev');
const storage = require('electron-json-storage')
const {
    HANDLE_SAVE_DATA, FETCH_DATA_FROM_STORAGE, HANDLE_FETCH_DATA, SAVE_DATA_IN_STORAGE, HANDLE_UPDATE_DATA,
    UPDATE_DATA_IN_STORAGE, REMOVE_DATA_IN_STORAGE, HANDLE_REMOVE_DATA
} = require("../src/utils/constants");

let win;
let itemsStorage;
let tab;

function createWindow() {
    // Create the browser window.
    win = new BrowserWindow({
        width: 1000,
        height: 700,
        webPreferences: {
            nodeIntegration: true,
            enableRemoteModule: true,
            contextIsolation: false
        },
    });
    // and load the index.html of the app.
    // win.loadFile("index.html");
    win.loadURL(
        isDev
            ? 'http://localhost:3000'
            : `file://${path.join(__dirname, '../build/index.html')}`
    );
    // Open the DevTools.
    if (isDev) {
        win.webContents.openDevTools({mode: 'detach'});
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});

ipcMain.on(FETCH_DATA_FROM_STORAGE, (event, message) => {
    //storage.clear(()=> {})
    storage.get(message, (error, data) => {
        itemsStorage = JSON.stringify(data) === '{}' ? [] : data;
        if (error) {
            win && win.send(HANDLE_FETCH_DATA, {
                success: false,
                message: "nothing returned"
            })
        } else {
            win && win.send(HANDLE_FETCH_DATA, {
                success: true,
                message: itemsStorage
            })
        }
    })
})

ipcMain.on(SAVE_DATA_IN_STORAGE, (event, message) => {
    console.log("main receive SAVE_DATA_IN_STORAGE", message)
    itemsStorage.push(message)
    storage.set('coffre', itemsStorage, (error) => {
        if (error) {
            console.log('error')
            win && win.send(HANDLE_SAVE_DATA, {
                success: false,
                message: "not saved"
            })
        } else {
            console.log('good')
            win && win.send(HANDLE_SAVE_DATA, {
                success: true,
                message: itemsStorage
            })
        }
    })
})

ipcMain.on(UPDATE_DATA_IN_STORAGE, (event, message) => {
    console.log("main receive UPDATE_DATA_IN_STORAGE", message)
    //storage.clear(() => {})
    tab = [];
    itemsStorage.map((value) => {
        if (value.id === message.id) {
            value = message;
        }
        tab.push(value)
    })
    itemsStorage = []
    itemsStorage = tab
    storage.clear(() => {
    })
    storage.set('coffre', itemsStorage, (error) => {
        if (error) {
            console.log('error')
            win && win.send(HANDLE_UPDATE_DATA, {
                success: false,
                message: "not saved"
            })
        } else {
            console.log('good')
            win && win.send(HANDLE_UPDATE_DATA, {
                success: true,
                message: itemsStorage
            })
        }
    })
})

ipcMain.on(REMOVE_DATA_IN_STORAGE, (event, message) => {
    console.log("main receive REMOVE_DATA_IN_STORAGE", message)
    //storage.clear(() => {})
    tab = [];
    itemsStorage.map((value) => {
        console.log(value)
        console.log(message)
        if (value.id !== message) {
            tab.push(value)
        }
    })
    itemsStorage = []
    itemsStorage = tab
    storage.clear(() => {
    })
    storage.set('coffre', itemsStorage, (error) => {
        if (error) {
            console.log('error')
            win && win.send(HANDLE_REMOVE_DATA, {
                success: false,
                message: "not saved"
            })
        } else {
            console.log('good')
            win && win.send(HANDLE_REMOVE_DATA, {
                success: true,
                message: itemsStorage
            })
        }
    })
})


